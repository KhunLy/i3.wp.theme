<!DOCTYPE html>
<!-- affiche l'attribut "lang" du site -->
<html <?php language_attributes() ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- affiche le nom du site et le titre de la page -->
    <title><?php bloginfo('name');wp_title('-') ?></title>
    <!-- affiche les différents "link" nécessaires à WP -->
    <?php wp_head() ?>
    <?php $base_url = get_stylesheet_directory_uri() ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="<?= $base_url ?>/style.css">
</head>
<body>
    <header>
        <nav>
            <?php wp_nav_menu([
                "theme_location" => "primary_menu",
                "container_id" => "menu-container",
                "menu_class" => "primary-menu"
            ]) ?>
        </nav>
    </header>
    <main>
        <!-- Sera différent selon les pages à afficher -->
    </main>
    <footer></footer>

    <!-- affiche les différents scripts nécessaires à WP -->
    <?php wp_footer() ?>
</body>
</html>